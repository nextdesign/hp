<?php
class Acceptable {
	// 受注可能性
	public static $list = array (
			"1" => "対応できます",
			"2" => "今は対応できません",
	);
	
	//テキスト表現を応答する
	public static function getText($key){
		$result = "";
		if (array_key_exists($key, Acceptable::$list)){
			$result = Acceptable::$list[$key];
		}
		return $result;
	}
}