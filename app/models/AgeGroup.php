<?php
class AgeGroup {
	// 年齢層
	public static $list = array (
			"15" => "20歳未満",
			"20" => "20代前半",
			"25" => "20代後半",
			"30" => "30代前半",
			"35" => "30代後半",
			"40" => "40代後半",
			"45" => "40代後半",
			"50" => "50代前半",
			"55" => "50代後半",
			"60" => "60代前半",
			"65" => "60代後半",
			"70" => "70代前半",
			"75" => "75歳以上",
	);
	
	//テキスト表現を応答する
	public static function getText($key){
		$result = "";
		if (array_key_exists($key, AgeGroup::$list)){
			$result = AgeGroup::$list[$key];
		}
		return $result;
	}
}