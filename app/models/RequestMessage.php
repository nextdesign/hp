<?php
/**
 * リクエストメッセージ
 */
class RequestMessage {
	
	private $requesterMailAddress = 0;
	private $engineerId = 0;
	private $engineerMailAddress = "";
	private $text1 = "";
	private $replyTo = "";
	
	public function validate(){
		if ($this->replyTo == ""){
			return "返信先を記載してください。";
		} else if(mb_strlen($this->replyTo) > 100){
			return "返信先は100文字以内です。";
		} else if ($this->text1 == ""){
			return "本文を記載してください。";
		} else if (mb_strlen($this->text1)  > 1000){
			return "本文は1000文字以内です。";
		}
		return "";
	}

	public function getReplyTo(){
		return $this->replyTo;
	}
	public function setReplyTo($replyTo){
		$this->replyTo= $replyTo;
	}
	
	public function getRequesterMailAddress(){
		return $this->requesterMailAddress;
	}
	public function setRequesterMailAddress($address){
		$this->requesterMailAddress= $address;
	}
	
	public function getEngineerId(){
		return $this->engineerId;
	}
	public function setEngineerId($eid){
		$this->engineerId= $eid;
	}

	public function getEngineerMailAddress(){
		return $this->engineerMailAddress;
	}
	public function setEngineerMailAddress($address){
		$this->engineerMailAddress= $address;
	}
	
	public function getText1(){
		return $this->text1;
	}
	public function setText1($text){
		$this->text1 = $text;
	}
	
}