<?php
require_once dirname ( __FILE__ ) . '/../util/Logger.php';
require_once dirname ( __FILE__ ) . '/../util/StringUtil.php';
require_once dirname ( __FILE__ ) . '/Acceptable.php';
require_once dirname ( __FILE__ ) . '/AgeGroup.php';
require_once dirname ( __FILE__ ) . '/Position.php';
require_once dirname ( __FILE__ ) . '/Prefecture.php';
/**
 * 技術者
 */
class Engineer {
	const MAX_LENGTH_DISPLAY_NAME = 50;
	const MAX_LENGTH_FREE_WORD = 250;
	
	private $id = 0;
	private $displayName = "";
	private $mailAddress = "";
	private $acceptable = "";
	private $position = "";
	private $ageGroup = "";
	private $prefecture = "40";
	private $cityName = "";
	private $freeWord = "";
	private $skillList = array ();
	
	// 基本情報サマリー
	public function getBasicInfo() {
		return $this->displayName . " [" . $this->getAddressText () . "][" . $this->getPositionText () . "][" . $this->getAgeGroupText () . "][" . $this->getAcceptableText () . "]";
	}
	
	// 受注可否状況テキストを応答する
	public function getAcceptableText() {
		return Acceptable::getText ( $this->acceptable );
	}

	// 立場テキストを応答する
	public function getPositionText() {
		return Position::getText ( $this->position );
	}
	
	// 年齢層テキストを応答する
	public function getAgeGroupText() {
		return AgeGroup::getText ( $this->ageGroup );
	}
	
	// 住所テキストを応答する
	public function getAddressText() {
		return Prefecture::getText ( $this->prefecture ) . " " . $this->cityName;
	}
	
	// 言語スキルリストを応答する
	public function getLanguageSkillList() {
		$resultList = array ();
		foreach ( $this->skillList as $skillId ) {
			if (Skill::isLanguageSkill ( $skillId )) {
				$resultList [] = Skill::getSkillName ( $skillId );
			}
		}
		return $resultList;
	}
	
	// OSスキルリストを応答する
	public function getOsSkillList() {
		$resultList = array ();
		foreach ( $this->skillList as $skillId ) {
			if (Skill::isOsSkill ( $skillId )) {
				$resultList [] = Skill::getSkillName ( $skillId );
			}
		}
		return $resultList;
	}
	
	// 役割スキルリストを応答する
	public function getRoleSkillList() {
		$resultList = array ();
		foreach ( $this->skillList as $skillId ) {
			if (Skill::isRoleSkill ( $skillId )) {
				$resultList [] = Skill::getSkillName ( $skillId );
			}
		}
		return $resultList;
	}
	// ドメインスキルリストを応答する
	public function getDomainSkillList() {
		$resultList = array ();
		foreach ( $this->skillList as $skillId ) {
			if (Skill::isDomainSkill ( $skillId )) {
				$resultList [] = Skill::getSkillName ( $skillId );
			}
		}
		return $resultList;
	}
	
	/**
	 * 必須入力項目などをチェックする
	 */
	public function validate() {
		$result = "";
		if ($this->displayName == "") {
			$result .= "表示名を入力してください。";
		} else if(mb_strlen($this->displayName) > self::MAX_LENGTH_DISPLAY_NAME){
			$result .= "表示名は" . self::MAX_LENGTH_DISPLAY_NAME . "文字以内です。";
		} else if(mb_strlen($this->freeWord) > self::MAX_LENGTH_FREE_WORD){
			$result .= "フリーワードは" .self::MAX_LENGTH_FREE_WORD . "文字以内です。";
		}
		return $result;
	}
	
	/**
	 * 新規登録の（本登録していない）技術者か否か
	 *
	 * @return boolean
	 */
	public function isNewEngineer() {
		return $this->id < 1;
	}
	
	/**
	 * 指定されたスキルを有しているか否か
	 *
	 * @param unknown $skillId        	
	 * @return boolean
	 */
	public function hasSkill($skillId) {
		return in_array ( $skillId, $this->skillList );
	}
	
	// コンストラクタ
	public function __construct($record = null) {
		if ($record != null && ! empty ( $record )) {
			$this->id = $record ["id"];
			$this->mailAddress = trim ( $record ["mail_address"] );
			$this->displayName = trim ( $record ["display_name"] );
			$this->acceptable = $record ["acceptable"];
			$this->position = $record ["position"];
			$this->ageGroup = $record ["age_group"];
			$this->prefecture = $record ["prefecture"];
			$this->cityName = trim ( $record ["city_name"] );
			$this->freeWord = trim ( $record ["free_word"] );
			
			if (isset ( $record ["skillList"] )) {
				foreach ( $record ["skillList"] as $skillId ) {
					$this->addSkill ( $skillId );
				}
			}
		}
	}
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		if ($id != null && $id != "") {
			$this->id = $id;
		}
	}
	public function getDisplayName() {
		return $this->displayName;
	}
	public function setDisplayName($displayName) {
		$this->displayName = $displayName;
	}
	public function getMailAddress() {
		return $this->mailAddress;
	}
	public function setMailAddress($mailAddress) {
		$this->mailAddress = $mailAddress;
	}
	
	// スキルリスト
	public function getSkillList() {
		return $this->skillList;
	}
	public function addSkill($skillId) {
		$this->skillList [] = $skillId;
	}
	public function clearSkillList() {
		$this->skillList = array ();
	}
	public function getAcceptable() {
		return $this->acceptable;
	}
	public function setAcceptable($acceptable) {
		if ($acceptable != null && $acceptable != "") {
			$this->acceptable = $acceptable;
		}
	}
	public function getPosition() {
		return $this->position;
	}
	public function setPosition($position) {
		if ($position != null && $position!= "") {
			$this->position = $position;
		}
	}
	public function getAgeGroup() {
		return $this->ageGroup;
	}
	public function setAgeGroup($ageGroup) {
		if ($ageGroup != null && $ageGroup != "") {
			$this->ageGroup = $ageGroup;
		}
	}
	public function getPrefecture() {
		return $this->prefecture;
	}
	public function setPrefecture($prefecture) {
		$this->prefecture = $prefecture;
	}
	public function getCityName() {
		return $this->cityName;
	}
	public function setCityName($cityName) {
		$this->cityName = $cityName;
	}
	public function getFreeWord() {
		return $this->freeWord;
	}
	
	// フリーワード
	public function setFreeWord($freeWord) {
		$this->freeWord = $freeWord;
	}
	
	// フリーワード 検索用UpperCase
	public function getFreeWordUppercase() {
		return StringUtil::toUpperCase ( $this->freeWord );
	}
}
