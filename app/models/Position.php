<?php
class Position {
	// 立場
	public static $list = array (
			"10" => "個人",
			"20" => "在籍中",
	);
	
	//テキスト表現を応答する
	public static function getText($key){
		$result = "";
		if (array_key_exists($key, Position::$list)){
			$result = Position::$list[$key];
		}
		return $result;
	}
}