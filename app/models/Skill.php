<?php
require_once dirname ( __FILE__ ) . '/../util/Logger.php';
class Skill {
	
	// スキル名を応答する（スキルカテゴリ名を含む）
	public static function getSkillName($skillId) {
		if (array_key_exists ( $skillId, Skill::$langeageList )) {
			return Skill::$langeageList [$skillId];
		}
		if (array_key_exists ( $skillId, Skill::$osList )) {
			return Skill::$osList [$skillId];
		}
		if (array_key_exists ( $skillId, Skill::$domainList )) {
			return Skill::$domainList [$skillId];
		}
		if (array_key_exists ( $skillId, Skill::$roleList )) {
			return Skill::$roleList [$skillId];
		}
	}
	
	//言語スキルか否か
	public static function isLanguageSkill($skillId){
		return 1000 < $skillId && $skillId < 2000;
	}

	//OSスキルか否か
	public static function isOsSkill($skillId){
		return 2000 < $skillId && $skillId < 3000;
	}
	
	//役割スキルか否か
	public static function isRoleSkill($skillId){
		return 3000 < $skillId && $skillId < 4000;
	}

	//ドメインスキルか否か
	public static function isDomainSkill($skillId){
		return 4000 < $skillId && $skillId < 5000;
	}
	
	// 言語
	public static $langeageList = array (
			1001 => "ABAP",
			1002 => "Ada",
			1003 => "AHDL",
			1004 => "ALGOL",
			1005 => "APL",
			1006 => "Atom",
			1007 => "AWK",
			1008 => "Bash",
			1009 => "BASIC",
			1010 => "BCPL",
			1011 => "C",
			1012 => "C#",
			1013 => "C++",
			1014 => "CAL",
			1015 => "CASL",
			1016 => "Clojure",
			1017 => "COBOL",
			1018 => "CoffeeScript",
			1019 => "CPL",
			1020 => "csh",
			1021 => "Curl",
			1022 => "Curry",
			1023 => "dBase",
			1024 => "Delphi",
			1025 => "ECMAScript",
			1026 => "Eiffel",
			1027 => "Erlang",
			1028 => "Escapade",
			1029 => "F#",
			1030 => "Forth",
			1031 => "FORTRAN",
			1032 => "Go",
			1033 => "Groovy",
			1034 => "Haskell",
			1035 => "IDL",
			1036 => "IPL",
			1037 => "J#",
			1038 => "Java",
			1039 => "JavaScript",
			1040 => "Julia",
			1041 => "Kotlin",
			1042 => "Lisp",
			1043 => "LOGO",
			1044 => "Lua",
			1045 => "MATLAB",
			1046 => "Objective Caml",
			1047 => "Objective-C",
			1048 => "Occam",
			1049 => "Pascal",
			1050 => "PBASIC",
			1051 => "Perl",
			1052 => "PHP",
			1053 => "Pic",
			1054 => "PL/I",
			1055 => "PowerBuilder",
			1056 => "PowerShell",
			1057 => "Prolog",
			1058 => "Python",
			1059 => "R",
			1060 => "REXX",
			1061 => "RPG (OS/400)",
			1062 => "Ruby",
			1063 => "SAS",
			1064 => "Scala",
			1065 => "Scheme",
			1066 => "Scratch",
			1067 => "Self",
			1068 => "sh",
			1069 => "Simula",
			1070 => "Smalltalk",
			1071 => "Squeak",
			1072 => "Swift",
			1073 => "Tcl",
			1074 => "tcsh",
			1075 => "TeX",
			1076 => "TypeScript",
			1077 => "VBScript",
			1078 => "Verilog HDL",
			1079 => "Visual Basic.NET",
			1080 => "その他" 
	);
	
	// OS
	public static $osList = array (
			2001 => "Amoeba",
			2002 => "Android",
			2003 => "BeRTOS",
			2004 => "BlackBerry",
			2005 => "BSD",
			2006 => "ChibiOS/RT",
			2007 => "Contiki",
			2008 => "CP/Q",
			2009 => "Darwin",
			2010 => "DragonFlyBSD",
			2011 => "eCos",
			2012 => "Enea OSE",
			2013 => "Firefox OS",
			2014 => "FreeBSD",
			2015 => "FreeRTOS",
			2016 => "Haiku",
			2017 => "INtime",
			2018 => "iOS",
			2019 => "iRMX",
			2020 => "ITRON",
			2021 => "Linux",
			2022 => "LynxOS",
			2023 => "Mac OSX/macOS",
			2024 => "Nucleus RTOS",
			2025 => "OpenBSD",
			2026 => "OS/2",
			2027 => "OS-9",
			2028 => "OSASK",
			2029 => "Palm OS",
			2030 => "PC DOS",
			2031 => "Plan 9",
			2032 => "PowerMAX OS",
			2033 => "pSOS",
			2034 => "QNX",
			2035 => "RedHawk Linux",
			2036 => "REX OS",
			2037 => "RSX-11",
			2038 => "RT-11",
			2039 => "RTEMS",
			2040 => "SkyOS",
			2041 => "Smalight OS",
			2042 => "THEOS",
			2043 => "T-Kernel",
			2044 => "TOPPERS",
			2045 => "Ubuntu Studio",
			2046 => "UNIX",
			2047 => "V7系",
			2048 => "VRTX",
			2049 => "VxWorks",
			2050 => "webOS",
			2051 => "Windows",
			2052 => "Windows CE",
			2053 => "Windows Mobile",
			2054 => "Windows Phone",
			2055 => "μITRON",
			2056 => "ホスト系：UNISYS",
			2057 => "ホスト系：IBM",
			2058 => "ホスト系：NEC",
			2059 => "ホスト系：東芝",
			2060 => "ホスト系：日立",
			2061 => "ホスト系：富士通",
			2062 => "ホスト系：その他",
			2063 => "その他" 
	);
	
	// 担当
	public static $roleList = array (
			3001 => "PM",
			3002 => "PL",
			3003 => "PMO",
			3004 => "要件定義",
			3005 => "設計",
			3006 => "実装",
			3007 => "テスト設計",
			3008 => "評価・テスター",
			3009 => "運用・保守",
			3010 => "データベース設計管理",
			3011 => "ネットワーク設計管理",
			3012 => "サーバ設計構築管理",
			3013 => "クラウド管理",
			3014 => "アーキテクト",
			3015 => "コンサルティング",
			3016 => "メンター",
			3017 => "ヘルプデスク",
			3018 => "インストラクタ",
			3019 => "デザイン",
			3020 => "研修講師",
			3021 => "その他" 
	);
	
	// 分野
	public static $domainList = array (
			4001 => "科学技術計算",
			4002 => "データ分析・統計",
			4003 => "AI・機械学習",
			4004 => "セキュリティ",
			4005 => "ネットワーク・通信",
			4006 => "組込み・制御系",
			4007 => "Web系",
			4008 => "スマホ系",
			4009 => "ゲーム系",
			4010 => "業務系-農林水産業",
			4011 => "業務系-鉱業",
			4012 => "業務系-建設業",
			4013 => "業務系-製造業",
			4014 => "業務系-電気・ガス・水道業",
			4015 => "業務系-情報通信業",
			4016 => "業務系-運輸業・郵便業",
			4017 => "業務系-卸売業・小売業",
			4018 => "業務系-金融業・保険業",
			4019 => "業務系",
			4020 => "その他" 
	);
}