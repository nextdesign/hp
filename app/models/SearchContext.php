<?php
require_once dirname ( __FILE__ ) . '/../util/StringUtil.php';
/**
 * 検索コンテキスト
 */
class SearchContext {
	
	// 条件
	private $skillList = array ();
	private $prefecture = "40";
	private $cityName = "";
	private $freeWord = "";
	private $displayName = "";
	
	// 結果表示関係
	const NAVIGATOR_WIDTH = 5;
	const PAGE_ROW_SIZE = 3;
	private $totalRowCount = 0;
	private $currentPageNo = 1;
	private $navigatorStartPageNo = 1; // ナビゲータの表示開始ページ番号
	                                   
	// 遷移状態
	private $newCondition = true;
	
	// 全行数
	public function getTotalRowCount() {
		return $this->totalRowCount;
	}
	public function setTotalRowCount($count) {
		$this->totalRowCount = $count;
	}
	
	//表示行番号（開始）
	public function getStartRowNo() {
		$result = ($this->currentPageNo - 1) * self::PAGE_ROW_SIZE + 1;
		if ($result > $this->totalRowCount) {
			$result = $this->totalRowCount;
		}
		return $result;
	}

	//表示行番号（終了）
	public function getEndRowNo() {
		$result = $this->currentPageNo * self::PAGE_ROW_SIZE;
		if ($result > $this->totalRowCount) {
			$result = $this->totalRowCount;
		}
		return $result;
	}
	
	// 現在のページ番号
	public function setCurrentPageNo($pageNo) {
		$this->currentPageNo = $pageNo;
	}
	public function getCurrentPageNo() {
		return $this->currentPageNo;
	}
	
	// ナビゲータを前に動かせるか否か
	public function hasPrevPage() {
		return $this->navigatorStartPageNo > 1;
	}
	
	// ナビゲータを次に動かせるか否か
	public function hasNextPage() {
		return ($this->navigatorStartPageNo + self::NAVIGATOR_WIDTH) <= $this->getMaxPageNo ();
	}
	
	// ナビゲータを前にずらす
	public function prevNavigator() {
		if ($this->hasPrevPage ()) {
			$this->navigatorStartPageNo -= 1;
		}
	}
	
	// ナビゲータを次にずらす
	public function nextNavigator() {
		if ($this->hasNextPage ()) {
			$this->navigatorStartPageNo += 1;
		}
	}
	
	// SELECT文のOFFSET値を応答する
	public function getOffsetForSelect() {
		return ($this->currentPageNo - 1) * self::PAGE_ROW_SIZE;
	}
	
	// 最大ページ番号（この条件で検索した場合の全行数をもとにした最大ページ番号）
	public function getMaxPageNo() {
		return ceil ( $this->getTotalRowCount () / self::PAGE_ROW_SIZE );
	}
	
	// ナビゲータの表示開始ページ番号
	public function getNavigatorStartPageNo() {
		return $this->navigatorStartPageNo;
	}
	public function setNavigatorStartPageNo($no) {
		$this->navigatorStartPageNo = $no;
	}
	
	// 条件が設定されているか否か（都道府県以外で）
	public function isValidCondition() {
		$result = false;
		if ($this->cityName != "" || $this->freeWord != "" || $this->displayName != "") {
			$result = true;
		} else if (count ( $this->skillList ) > 0) {
			$result = true;
		}
		return $result;
	}
	
	// 新規の検索か否か
	public function isNewCondition() {
		return $this->newCondition;
	}
	public function setNewCondition($newCondition) {
		$this->newCondition = $newCondition;
	}
	
	// スキル
	public function getSkillList() {
		return $this->skillList;
	}
	public function addSkill($skillId) {
		$this->skillList [] = $skillId;
	}
	public function clearSkillList() {
		$this->skillList = array ();
	}
	
	// 指定されたスキルを有しているか否か
	public function hasSkill($skillId) {
		return in_array ( $skillId, $this->skillList );
	}
	
	// 表示名
	public function getDisplayName() {
		return $this->displayName;
	}
	public function setDisplayName($displayName) {
		$this->displayName = $displayName;
	}
	
	// フリーワード
	public function getFreeWord() {
		return $this->freeWord;
	}
	
	// フリーワード
	public function setFreeWord($freeWord) {
		$this->freeWord = $freeWord;
	}
	
	// フリーワード 検索用UpperCase
	public function getFreeWordUppercase() {
		return StringUtil::toUpperCase ( $this->freeWord );
	}
	
	// 都道府県
	public function getPrefecture() {
		return $this->prefecture;
	}
	public function setPrefecture($prefecture) {
		$this->prefecture = $prefecture;
	}
	
	// 市区町村
	public function getCityName() {
		return $this->cityName;
	}
	public function setCityName($cityName) {
		$this->cityName = $cityName;
	}
}