<?php
require_once dirname(__FILE__) . '/../util/Logger.php';
require_once dirname(__FILE__) . '/ServiceBase.php';
require_once dirname(__FILE__) . '/../managers/EngineerManager.php';

class EngineerService extends ServiceBase{
	
	public function countTotalRows($searchContext) {
		$result = 0;
		$engineerManager = new EngineerManager ();
		try {
			$pdo = $this->getPDO ();
			$result = $engineerManager->countTotalRows ( $searchContext, $pdo );
			$pdo = null;
		} catch ( PDOException $ex ) {
			$pdo = null;
			Logger::put ( "件数カウント NG." . $ex->getMessage () );
		} catch ( Exception $ex ) {
			$pdo = null;
			Logger::put ( "EngineerService#selectCount NG." . $ex->getMessage () );
		}
		return $result;
	}
	
	/**
	 * 検索する
	 * @param unknown $searcContext
	 * @return unknown|string
	 */
	public function search($searchContext){
		$resultList = array();
		$engineerManager = new EngineerManager ();
		try {
			$pdo =  $this->getPDO();
			$resultList= $engineerManager->search( $searchContext, $pdo );
			$pdo = null;
		} catch ( PDOException $ex ) {
			$pdo = null;
			Logger::put("検索 NG." . $ex->getMessage ()) ;
		} catch(Exception $ex){
			$pdo = null;
			Logger::put("EngineerService#search NG." . $ex->getMessage ()) ;
		}
		return $resultList;
	}
	
	/**
	 * 削除する
	 * @param unknown $engineer
	 * @return boolean
	 */
	public function deleteEngineer($engineer){
		$result = false;
		$engineerManager = new EngineerManager ();
		try {
			$pdo = $this->getPDO ();
			$pdo->beginTransaction ();
			$result = $engineerManager->delete( $engineer, $pdo );
			$pdo->commit ();
			Logger::put("削除 OK.") ;
			$pdo = null;
		} catch ( PDOException $ex ) {
			Logger::put("削除 NG." . $ex->getMessage ()) ;
			$pdo->rollBack ();
			$pdo = null;
		} catch(Exception $ex){
			Logger::put("EngineerService#addEngineer NG." . $ex->getMessage ()) ;
			$pdo = null;
		}
		return $result;
	}
	
	/**
	 * 登録する
	 * @param unknown $engineer
	 * @return unknown
	 */
	public function addOrReplaceEngineer($engineer){
		$updatedEngineer = null;
		$engineerManager = new EngineerManager ();
		try {
			$pdo = $this->getPDO ();
			$pdo->beginTransaction ();
			$updatedEngineer = $engineerManager->addOrReplace ( $engineer, $pdo );
			$pdo->commit ();
			Logger::put("本登録 OK.") ;
			$pdo = null;
		} catch ( PDOException $ex ) {
			Logger::put("本登録 NG." . $ex->getMessage ()) ;
			$pdo->rollBack ();
			$pdo = null;
		} catch(Exception $ex){
			Logger::put("EngineerService#addEngineer NG." . $ex->getMessage ()) ;
			$pdo = null;
		}
		return $updatedEngineer;
	}
	
	/**
	 * メールアドレスで登録済みエンジニアを取得する
	 * @param unknown $mailAddress
	 * @return Engineer
	 */
	public function getEngineerByMailAddress($mailAddress){
		$result = null;
		$engineerManager = new EngineerManager ();
		try {
			$pdo = $this->getPDO ();
			$result = $engineerManager->getEngineerByMailAddress( $mailAddress, $pdo );
			$pdo = null;
			Logger::put("mailAddressでEngineerを取得 OK.") ;
		} catch ( PDOException $ex ) {
			$pdo = null;
			Logger::put("mailAddressでEngineerを取得 NG." . $ex->getMessage ()) ;
		} catch(Exception $ex){
			$pdo = null;
			Logger::put("EngineerService#getEngineerByMailAddress NG." . $ex->getMessage ()) ;
		}
		return $result;
	}
	
	/**
	 * engineerIdで取得する
	 * @param unknown $eid
	 * @return unknown
	 */
	public function getEngineerById($eid){
		$result = null;
		$engineerManager = new EngineerManager ();
		try {
			$pdo = $this->getPDO ();
			$result = $engineerManager->getEngineerById( $eid, $pdo );
			$pdo = null;
			Logger::put("idでEngineerを取得 OK.") ;
		} catch ( PDOException $ex ) {
			$pdo = null;
			Logger::put("idでEngineerを取得 NG." . $ex->getMessage ()) ;
		} catch(Exception $ex){
			$pdo = null;
			Logger::put("EngineerService#getEngineerById NG." . $ex->getMessage ()) ;
		}
		return $result;
	}
	
	/**
	 * 仮登録キーからメールアドレスを取得する
	 * @param unknown $temporaryKey
	 * @return string|unknown　メールアドレス 空の時は取得失敗
	 */
	public function getMailAddressByTemporaryKey($temporaryKey){
		$result = "";
		$engineerManager = new EngineerManager ();
		try {
			$pdo = $this->getPDO ();
			$result = $engineerManager->getMailAddressByTemporaryKey( $temporaryKey, $pdo );
			$pdo = null;
			Logger::put("仮登録キーでmailAddress取得 OK.") ;
		} catch ( PDOException $ex ) {
			$pdo = null;
			Logger::put("仮登録キーでmailAddress取得 NG." . $ex->getMessage ()) ;
		} catch(Exception $ex){
			$pdo = null;
			Logger::put("EngineerService#getMailAddressByTemporaryKey NG." . $ex->getMessage ()) ;
		}
		return $result;
	}
	
	/**
	 * 仮登録する
	 * @used-by registration/mail.php
	 * @param unknown $mailAddress 入力メールアドレス
	 * @return NULL|mixed　仮登録キー
	 */
	public function addEngineerTemporary($mailAddress) {
		$temporaryKey = "";
		$engineerManager = new EngineerManager ();
		try {
			$pdo = $this->getPDO ();
			$pdo->beginTransaction ();
			$temporaryKey = $engineerManager->addTemporary ( $mailAddress, $pdo );
			$pdo->commit ();
			$pdo = null;
			Logger::put("仮登録 OK.") ;
		} catch ( PDOException $ex ) {
			$pdo->rollBack ();
			$pdo = null;
			Logger::put("仮登録 NG." . $ex->getMessage ()) ;
		} catch(Exception $ex){
			$pdo = null;
			Logger::put("EngineerService#addEngineerTemporary NG." . $ex->getMessage ()) ;
		}
		return $temporaryKey;
	}
	
	/**
	 * 保管期限を過ぎた仮登録情報を削除する
	 * @return void
	 */
	public function clearExpiratedTemporayEngineer(){
		$engineerManager = new EngineerManager ();
		try {
			$pdo = $this->getPDO ();
			$pdo->beginTransaction ();
			$engineerManager->clearExpiratedTemporayEngineer( $pdo );
			$pdo->commit ();
			$pdo = null;
			Logger::put("保管期限を過ぎた仮登録情報を削除する OK.") ;
		} catch ( PDOException $ex ) {
			$pdo->rollBack ();
			$pdo = null;
			Logger::put("保管期限を過ぎた仮登録情報を削除する NG." . $ex->getMessage ()) ;
		} catch(Exception $ex){
			$pdo = null;
			Logger::put("EngineerService#clearExpiratedTemporayEngineer NG." . $ex->getMessage ()) ;
		}
	}
}