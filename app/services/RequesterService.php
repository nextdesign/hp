<?php
require_once dirname(__FILE__) . '/../util/Logger.php';
require_once dirname(__FILE__) . '/ServiceBase.php';
require_once dirname(__FILE__) . '/../managers/RequesterManager.php';

class RequesterService extends ServiceBase{
	
	public function getMailAddressAndEngineerIdByTemporaryKey($temporaryKey){
		$result = "";
		$requesterManager = new requesterManager ();
		try {
			$pdo = $this->getPDO ();
			$result = $requesterManager->getMailAddressAndEngineerIdByTemporaryKey( $temporaryKey, $pdo );
			$pdo = null;
			Logger::put("仮登録キーでmailAddressとEngineerId取得 OK.") ;
		} catch ( PDOException $ex ) {
			$pdo = null;
			Logger::put("仮登録キーでmailAddressとEngineerId取得 NG." . $ex->getMessage ()) ;
		} catch(Exception $ex){
			$pdo = null;
			Logger::put("EngineerService#getMailAddressAndEngineerIdByTemporaryKey NG." . $ex->getMessage ()) ;
		}
		return $result;
	}
	
	/**
	 * 期限を過ぎた仮登録情報を削除する
	 * @return void
	 */
	public function clearExpiratedTemporayRequester(){
		$requesterManager = new RequesterManager();
		try {
			$pdo = $this->getPDO ();
			$pdo->beginTransaction ();
			$requesterManager->clearExpiratedTemporayRequester( $pdo );
			$pdo->commit ();
			$pdo = null;
			Logger::put("保管期限を過ぎた仮登録情報を削除する OK.") ;
		} catch ( PDOException $ex ) {
			$pdo->rollBack ();
			$pdo = null;
			Logger::put("保管期限を過ぎた仮登録情報を削除する NG." . $ex->getMessage ()) ;
		} catch(Exception $ex){
			$pdo = null;
			Logger::put("RequesterService#clearExpiratedTemporayRequester NG." . $ex->getMessage ()) ;
		}
	}
	
	public function addRequesterTemporary($requesterMailAddress, $engineer_id) {
		$temporaryKey = "";
		$requesterManager = new RequesterManager ();
		try {
			$pdo = $this->getPDO ();
			$pdo->beginTransaction ();
			$temporaryKey = $requesterManager->addTemporary ( $requesterMailAddress, $engineer_id, $pdo );
			$pdo->commit ();
			$pdo = null;
			Logger::put("仮登録 OK.") ;
		} catch ( PDOException $ex ) {
			$pdo->rollBack ();
			$pdo = null;
			Logger::put("仮登録 NG." . $ex->getMessage ()) ;
		} catch(Exception $ex){
			$pdo = null;
			Logger::put("RequesterService#addRequesterTemporary NG." . $ex->getMessage ()) ;
		}
		return $temporaryKey;
	}
}