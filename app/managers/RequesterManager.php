<?php
require_once dirname ( __FILE__ ) . '/ManagerBase.php';

/**
 * 依頼者管理者
 */
class RequesterManager extends ManagerBase {
	
	public function getMailAddressAndEngineerIdByTemporaryKey($temporaryKey, $pdo) {
		$sql = "SELECT mail_address, engineer_id FROM requester_temporary WHERE temporary_key=?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $temporaryKey );
		$stmt->execute ();
		$result = $stmt->fetch ();
		if ($result != false) {
			return $result;
		} else {
			return "";
		}
	}
		
	public function addTemporary($requesterMailAddress, $engineer_id, $pdo) {
		if ($this->existsAsTemporary ( $requesterMailAddress, $pdo )) {
			$this->deleteFromTemporary ( $requesterMailAddress, $pdo );
		}
		return $this->insertTemporary ( $requesterMailAddress, $engineer_id, $pdo );
	}
	
	// 仮登録用の新しい技術者ID（ランダムな文字列）を応答する
	private static function newTemporaryKey() {
		// $str = array_merge ( range ( 'a', 'z' ), range ( '0', '9' ), range ( 'A', 'Z' ) );
		// SQLインジェクション対策として数字のみにした
		$str = array_merge ( range ( '1', '9' ) );
		$result = null;
		for($i = 0; $i < parent::TEMPORARY_ID_LENGTH; $i ++) {
			$result .= $str [rand ( 0, count ( $str ) - 1 )];
		}
		return $result;
	}
	
	// DBに仮登録する
	private function insertTemporary($requesterMailAddress, $engineer_id, $pdo) {
		$sql = "INSERT INTO requester_temporary(mail_address, temporary_key, engineer_id, created_at) VALUES (?,?,?,?)";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $requesterMailAddress);
		$temporaryKey = RequesterManager::newTemporaryKey ();
		$stmt->bindValue ( 2, $temporaryKey );
		$stmt->bindValue ( 3, $engineer_id );
		$stmt->bindValue ( 4, date ( parent::DATE_FORMAT ) );
		$stmt->execute ();
		return $temporaryKey;
	}
	
	// 仮登録されているか否か
	private function existsAsTemporary($requesterMailAddress, $pdo) {
		$sql = "SELECT count(mail_address) FROM requester_temporary WHERE mail_address = ?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $requesterMailAddress);
		$stmt->execute ();
		$count = $stmt->fetchColumn ();
		return $count > 0;
	}
	
	// 仮登録者から削除する
	private function deleteFromTemporary($requesterMailAddress, $pdo) {
		$sql = "DELETE FROM requester_temporary WHERE mail_address = ?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $requesterMailAddress);
		$stmt->execute ();
	}
	
	/**
	 * 保管期限を過ぎた仮登録技術者情報をすべて削除する
	 *
	 * @param unknown $pdo
	 * @return void
	 */
	public function clearExpiratedTemporayRequester($pdo) {
		$sql = "DELETE FROM requester_temporary WHERE created_at < ?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $this->getTemporaryExpirationDateTime () );
		$stmt->execute ();
	}
	
	// 仮登録の期限日時(6時間前)を応答する
	private function getTemporaryExpirationDateTime() {
		return date ( parent::DATE_FORMAT, strtotime ( "-6 hour" ) );
	}
}
