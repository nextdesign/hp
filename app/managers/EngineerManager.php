<?php
require_once dirname ( __FILE__ ) . '/../util/StringUtil.php';
require_once dirname ( __FILE__ ) . '/ManagerBase.php';
require_once dirname ( __FILE__ ) . '/../models/Engineer.php';
require_once dirname ( __FILE__ ) . '/../models/SearchContext.php';
require_once dirname ( __FILE__ ) . '/../models/Prefecture.php';
const KEYWORD_AND = " AND";
const KEYWORD_OR = " OR";

/**
 * 技術者管理者
 */
class EngineerManager extends ManagerBase {
	
	// SearchContextからWHERE句を作成する
	private function buildWhereClause($searchContext) {
		$where = "";
		// 基本情報
		if ($searchContext->getDisplayName ()) { // 2017.10.25時点では表示名は検索条件にしない
			$where .= KEYWORD_AND . " display_name='{$searchContext->getDisplayName ()}'";
		}
		if (! Prefecture::isUnSelected ( $searchContext->getPrefecture () )) {
			$where .= KEYWORD_AND . " prefecture='{$searchContext->getPrefecture ()}'";
		}
		if ($searchContext->getCityName ()) {
			$where .= KEYWORD_AND . " city_name LIKE '%{$searchContext->getCityName ()}%'";
		}
		if ($searchContext->getFreeWord ()) {
			$freeWord = $searchContext->getFreeWordUppercase ();
			$splitted = explode ( ',', $freeWord );
			if (count ( $splitted ) > 0) {
				$temp = trim ( $splitted [0] );
				$where .= KEYWORD_AND . " ((free_word_uppercase LIKE '%{$temp}%')";
				for($index = 1; $index < count ( $splitted ); $index ++) {
					$temp = trim ( $splitted [$index] );
					$where .= KEYWORD_OR . " (free_word_uppercase LIKE '%{$temp}%')";
				}
				$where .= ")";
			}
		}
		if (StringUtil::startsWith ( $where, KEYWORD_AND )) {
			$where = mb_substr ( $where, mb_strlen ( KEYWORD_AND ) );
		}
		// スキル
		$sidList = $searchContext->getSkillList ();
		if (count ( $sidList ) > 0) {
			$skillSelect = " id IN (SELECT engineer_id FROM engineer_skill WHERE skill_id IN (";
			foreach ( $sidList as $sid ) {
				$skillSelect .= "'{$sid}',";
			}
			if (StringUtil::endsWith ( $skillSelect, "," )) {
				$skillSelect = mb_substr ( $skillSelect, 0, (mb_strlen ( $skillSelect ) - 1) );
			}
			$skillSelect .= "))";
			if ($where == "") {
				$where .= $skillSelect;
			} else {
				$where .= KEYWORD_AND . $skillSelect;
			}
		}
		return $where;
	}
	
	// 件数
	public function countTotalRows($searchContext, $pdo) {
		$where = $this->buildWhereClause ( $searchContext );
		$sql = "SELECT count(id) FROM engineer WHERE " . $where;
		$stmt = $pdo->prepare ( $sql );
		$stmt->execute ();
		$count = $stmt->fetchColumn ();
		Logger::put ( $sql );
		return $count;
	}
	
	// 検索条件を満たす技術者リストを取得する
	public function search($searchContext, $pdo) {
		$resultList = array ();
		$where = $this->buildWhereClause ( $searchContext );
		$sql = "SELECT * FROM engineer WHERE " . $where . " ORDER BY updated_at DESC LIMIT " . SearchContext::PAGE_ROW_SIZE . " OFFSET {$searchContext->getOffsetForSelect()}";
		Logger::put ( $sql );
		$stmt = $pdo->prepare ( $sql );
		$stmt->execute ();
		while ( $result = $stmt->fetch ( PDO::FETCH_ASSOC ) ) {
			$engineer = new Engineer ( $result );
			$skillList = $this->getSkillListByEngineerId ( $engineer->getId (), $pdo );
			foreach ( $skillList as $skillId ) {
				$engineer->addSkill ( $skillId );
			}
			$resultList [] = $engineer;
		}
		return $resultList;
	}
	
	/**
	 * 技術者を削除する
	 *
	 * @param unknown $engineer        	
	 * @param unknown $pdo        	
	 */
	public function delete($engineer, $pdo) {
		$result = true;
		// engineer
		$sql = "DELETE FROM engineer WHERE id=?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $engineer->getId () );
		$stmt->execute ();
		// engineer_skill
		$sql = "DELETE FROM engineer_skill WHERE engineer_id=?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $engineer->getId () );
		$stmt->execute ();
		return $result;
	}
	
	/**
	 * 登録する
	 *
	 * @param unknown $engineer        	
	 * @param unknown $pdo        	
	 * @return unknown
	 */
	public function addOrReplace($engineer, $pdo) {
		if ($engineer->getId () > 0) {
			// 既存は削除,idはそのまま使用
			$this->delete ( $engineer, $pdo );
		} else {
			// 新規はid割り付け
			$id = EngineerManager::newId ( $pdo );
			$engineer->setId ( $id );
		}
		return $this->insert ( $engineer, $pdo );
	}
	
	// DBに本登録する
	private function insert($engineer, $pdo) {
		// engineer
		$sql = "INSERT INTO engineer(id, display_name, mail_address, age_group, acceptable, prefecture, city_name, free_word, free_word_uppercase, updated_at, position) 
		VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $engineer->getId () );
		$stmt->bindValue ( 2, $engineer->getDisplayName () );
		$stmt->bindValue ( 3, $engineer->getMailAddress () );
		$stmt->bindValue ( 4, $engineer->getAgeGroup () );
		$stmt->bindValue ( 5, $engineer->getAcceptable () );
		$stmt->bindValue ( 6, $engineer->getPrefecture () );
		$stmt->bindValue ( 7, $engineer->getCityName () );
		$stmt->bindValue ( 8, $engineer->getFreeWord () );
		$stmt->bindValue ( 9, $engineer->getFreeWordUppercase () );
		$stmt->bindValue ( 10, date ( parent::DATE_FORMAT ) );
		$stmt->bindValue ( 11, $engineer->getPosition () ); //2018.1.2 add
		$stmt->execute ();
		// engineer_skill
		$sql = "INSERT INTO engineer_skill(engineer_id, skill_id) VALUES (?,?)";
		$stmt = $pdo->prepare ( $sql );
		foreach ( $engineer->getSkillList () as $skillId ) {
			$stmt->bindValue ( 1, $engineer->getId () );
			$stmt->bindValue ( 2, $skillId );
			$stmt->execute ();
		}
		return $engineer;
	}
	
	// 本登録用の新しい技術者IDを応答する
	private static function newId($pdo) {
		$sql = "UPDATE engineer_sequence SET id=LAST_INSERT_ID(id+1)";
		$stmt = $pdo->prepare ( $sql );
		$stmt->execute ();
		$sql = "SELECT LAST_INSERT_ID()";
		$stmt = $pdo->prepare ( $sql );
		$stmt->execute ();
		$nextId = $stmt->fetch ();
		return $nextId [0];
	}
	
	/**
	 * メールアドレスで登録済みエンジニアを取得する
	 *
	 * @param unknown $mailAddress        	
	 * @param unknown $pdo        	
	 * @return Engineer|NULL 存在しない場合はnull
	 */
	public function getEngineerByMailAddress($mailAddress, $pdo) {
		$resultEngineer = null;
		$sql = "SELECT * FROM engineer WHERE mail_address=?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $mailAddress );
		$stmt->execute ();
		$result = $stmt->fetch ();
		if ($result != false) {
			$resultEngineer = new Engineer ( $result );
			$skillList = $this->getSkillListByEngineerId ( $resultEngineer->getId (), $pdo );
			foreach ( $skillList as $skillId ) {
				$resultEngineer->addSkill ( $skillId );
			}
		}
		return $resultEngineer;
	}
	
	/**
	 * engineeridで取得する
	 * @param unknown $eid
	 * @param unknown $pdo
	 * @return NULL|Engineer
	 */
	public function getEngineerById($eid, $pdo) {
		$resultEngineer = null;
		$sql = "SELECT * FROM engineer WHERE id=?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $eid );
		$stmt->execute ();
		$result = $stmt->fetch ();
		if ($result != false) {
			$resultEngineer = new Engineer ( $result );
			$skillList = $this->getSkillListByEngineerId ( $resultEngineer->getId (), $pdo );
			foreach ( $skillList as $skillId ) {
				$resultEngineer->addSkill ( $skillId );
			}
		}
		return $resultEngineer;
	}
	
	// 技術者idでスキルリストを応答する
	private function getSkillListByEngineerId($eid, $pdo) {
		$resultList = array ();
		$sql = "SELECT * FROM engineer_skill WHERE engineer_id=?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $eid );
		$stmt->execute ();
		while ( $row = $stmt->fetch () ) {
			$resultList [] = $row ["skill_id"];
		}
		return $resultList;
	}
	
	/**
	 * 仮登録キーでメールアドレスを取得する
	 *
	 * @param unknown $temporaryKey        	
	 * @param unknown $pdo        	
	 * @return unknown|string メールアドレス
	 */
	public function getMailAddressByTemporaryKey($temporaryKey, $pdo) {
		$sql = "SELECT mail_address FROM engineer_temporary WHERE temporary_key=?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $temporaryKey );
		$stmt->execute ();
		$result = $stmt->fetch ();
		if ($result != false) {
			return $result [0];
		} else {
			return "";
		}
	}
	
	/**
	 * 仮登録する
	 *
	 * @param unknown $mailAddress
	 *        	入力メールアドレス
	 * @param unknown $pdo        	
	 * @return NULL|mixed 仮登録キー
	 */
	public function addTemporary($mailAddress, $pdo) {
		if ($this->existsAsTemporary ( $mailAddress, $pdo )) {
			$this->deleteFromTemporary ( $mailAddress, $pdo );
		}
		return $this->insertTemporary ( $mailAddress, $pdo );
	}
	
	// 仮登録用の新しい技術者ID（ランダムな文字列）を応答する
	private static function newTemporaryKey() {
		// $str = array_merge ( range ( 'a', 'z' ), range ( '0', '9' ), range ( 'A', 'Z' ) );
		// SQLインジェクション対策として数字のみにした
		$str = array_merge ( range ( '1', '9' ) );
		$result = null;
		for($i = 0; $i < parent::TEMPORARY_ID_LENGTH; $i ++) {
			$result .= $str [rand ( 0, count ( $str ) - 1 )];
		}
		return $result;
	}
	
	// DBに仮登録する
	private function insertTemporary($mailAddress, $pdo) {
		$sql = "INSERT INTO engineer_temporary(mail_address, temporary_key, created_at) VALUES (?,?,?)";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $mailAddress );
		$temporaryKey = EngineerManager::newTemporaryKey ();
		$stmt->bindValue ( 2, $temporaryKey );
		$stmt->bindValue ( 3, date ( parent::DATE_FORMAT ) );
		$stmt->execute ();
		return $temporaryKey;
	}
	
	// 仮登録されているか否か
	private function existsAsTemporary($mailAddress, $pdo) {
		$sql = "SELECT count(mail_address) FROM engineer_temporary WHERE mail_address = ?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $mailAddress );
		$stmt->execute ();
		$count = $stmt->fetchColumn ();
		return $count > 0;
	}
	
	// 仮登録者から削除する
	private function deleteFromTemporary($mailAddress, $pdo) {
		$sql = "DELETE FROM engineer_temporary WHERE mail_address = ?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $mailAddress );
		$stmt->execute ();
	}
	
	/**
	 * 保管期限を過ぎた仮登録技術者情報をすべて削除する
	 *
	 * @param unknown $pdo        	
	 * @return void
	 */
	public function clearExpiratedTemporayEngineer($pdo) {
		$sql = "DELETE FROM engineer_temporary WHERE created_at < ?";
		$stmt = $pdo->prepare ( $sql );
		$stmt->bindValue ( 1, $this->getTemporaryExpirationDateTime () );
		$stmt->execute ();
	}
	
	// 仮登録の期限日時(6時間前)を応答する
	private function getTemporaryExpirationDateTime() {
		return date ( parent::DATE_FORMAT, strtotime ( "-6 hour" ) );
	}
}
