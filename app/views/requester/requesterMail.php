<?php
session_start ();
require_once dirname ( __FILE__ ) . '/../../Constants.php';
require_once dirname ( __FILE__ ) . '/../Keys.php';
require_once dirname ( __FILE__ ) . '/../../util/Logger.php';
require_once dirname ( __FILE__ ) . '/../../services/RequesterService.php';
require_once dirname ( __FILE__ ) . '/../../models/Engineer.php';
require_once dirname ( __FILE__ ) . '/../../mail/Mailer.php';
$message = "";
$error = false;
$requesterMailAddress = $_SESSION [Keys::REQUESTER_MAIL_ADDRESS];
unset ( $_SESSION [Keys::REQUESTER_MAIL_ADDRESS] );
$engineer_id = $_SESSION [Keys::CONTACT_TO];
unset ( $_SESSION [Keys::CONTACT_TO] );

// 仮登録
$requesterService= new RequesterService();
$temporayKey = $requesterService->addRequesterTemporary ( $requesterMailAddress, $engineer_id );
Logger::put ( "temporayId=" . $temporayKey );

// メール送信
if ($temporayKey != "") {
	$mailer = new Mailer ();
	$result = $mailer->sendRequesterMessage( $requesterMailAddress, $temporayKey );
	if ($result) {
		$message = "メールを送信しました。{$requesterMailAddress}<br>メール本文のリンクから問合せ送信画面にアクセスしてください。<br>【注意】メール本文にURLリンクを記載していますので、環境によっては迷惑メールに分類されることがあります。" ;
	} else {
		$error = true;
		$temp = htmlspecialchars ( $engineer->getMailAddress() );
		$message = "メールを送信できませんでした。メールアドレスをご確認ください。" . $temp;
	}
} else {
	$temp = htmlspecialchars ( $engineer->getMailAddress() );
	$message = "メールを送信できません。" . $temp;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta content="" name="description">
<title><?php echo Constants::SITE_NAME ?> 依頼者画面URL送信完了</title>
<link rel="stylesheet" type="text/css" href="/app/style.css" media="screen"
	title="Stylesheet" />
<!-- VIEW PORT 2018.3.6 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
	<h1><?php echo Constants::SITE_NAME ?> 依頼者画面URL送信完了</h1>
	<div>
<?php
if ($error){
	echo "<div style=\"color:red;\">{$message}</div><br>";
} else {
	echo "<div>{$message}</div><br>";
}
?>
	</div>
<!-- FOOTER -->
<div id="ft">
&nbsp;<a href="../../index.html" target="_blank"><font color="white">トップ</font></a>
&nbsp;<a href="http://www.nextdesign.co.jp/" target="_blank"><font color="white">運営者</font></a>
&nbsp;<a href="kiyaku.html" target="_blank"><font color="white">利用規約</font></a>
</div>
</body>
</html>
