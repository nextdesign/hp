<?php
require_once dirname ( __FILE__ ) . '/../../Constants.php';
require_once dirname ( __FILE__ ) . '/../Keys.php';
require_once dirname(__FILE__) . '/../../mail/MailAddress.php';
session_start ();
unset ( $_SESSION [Keys::REQUESTER_MAIL_ADDRESS] );
$message = "";
$error = false;
$requesterMailAddress = "";
if ($_GET != null && isset($_GET[Keys::CONTACT_TO])){
	//検索結果リストから遷移してきたときのみ
	$_SESSION [Keys::CONTACT_TO] = $_GET[Keys::CONTACT_TO];
}
if ($_POST != null && isset($_POST [Keys::REQUESTER_MAIL_ADDRESS])) {
	//メールを受取るボタンが押下されたとき
	$requesterMailAddress= trim($_POST [Keys::REQUESTER_MAIL_ADDRESS]);
	$message = MailAddress::validate ( $requesterMailAddress);
	if ($message == "") {
		$_SESSION [Keys::REQUESTER_MAIL_ADDRESS] = $requesterMailAddress;
		$url = "./requesterMail.php";
		header ( "Location:{$url}" );
		exit ();
	} else {
		$error = true;
	}
} else {
	//画面を表示するのみ
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta content="" name="description">
<title><?php echo Constants::SITE_NAME ?> 依頼者画面URL送信</title>
<link rel="stylesheet" type="text/css" href="/app/style.css" media="screen"
	title="Stylesheet" />
<!-- VIEW PORT 2018.3.6 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
	<h1><?php echo Constants::SITE_NAME ?> 依頼者画面URL送信</h1>
	<div>
<?php
if ($message != "") {
	echo "<div style=\"color:red;\">{$message}</div><br>";
}
?>
    <div>
		下のテキストボックスにメールアドレスを入力し、「メールを受け取る」ボタンを押下します。<br>
		受信したメールの本文に、問合せ内容入力画面のURLが記載されています。<br>
		そのURLから問合せ内容入力画面にアクセスしてください。<br>
	</div>

	<form name="form1" method="post" action="contact.php">
		<p>
		<?php 
		if ($error){
			print ("<font color=\"red\">メールアドレス： <input type=\"text\" name=\"" . Keys::REQUESTER_MAIL_ADDRESS . "\" value=\"{$requesterMailAddress}\" style=\"width:70%; box-sizing:border-box\"></font>");
		} else {
			print ("メールアドレス： <input type=\"text\" name=\"" . Keys::REQUESTER_MAIL_ADDRESS . "\" value=\"{$requesterMailAddress}\" style=\"width:70%; box-sizing:border-box\">");
		}
			?>
		</p>
		<div>
			<button type="submit">メールを受け取る</button>
			<button type="button" onClick="location.href='/app/index.html'">キャンセル</button>
		</div>
	</form>
</div>
<!-- FOOTER -->
<div id="ft">
&nbsp;<a href="../../index.html" target="_blank"><font color="white">トップ</font></a>
&nbsp;<a href="http://www.nextdesign.co.jp/" target="_blank"><font color="white">運営者</font></a>
&nbsp;<a href="kiyaku.html" target="_blank"><font color="white">利用規約</font></a>
</div>
</body>
</html>
