<?php
class Keys {
	const ENGINEER = "engineer";
	const REQUEST_MESSAGE = "request_massage";
	const SEARCH_CONTEXT = "searchContext";
	const ENGINEER_MAIL_ADDRESS = "engineerMailAddress";
	const REQUESTER_MAIL_ADDRESS = "requesterMailAddress";
	const CONTACT_TO = "c";
	const TEMP_KEY = "k";
}
