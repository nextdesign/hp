<?php
require_once dirname ( __FILE__ ) . '/../../Constants.php';
require_once dirname ( __FILE__ ) . '/../Keys.php';
require_once dirname ( __FILE__ ) . '/../../util/Logger.php';
require_once dirname ( __FILE__ ) . '/../../util/StringUtil.php';
require_once dirname ( __FILE__ ) . '/../../models/Engineer.php';
require_once dirname ( __FILE__ ) . '/../../models/Skill.php';
require_once dirname ( __FILE__ ) . '/../../models/Prefecture.php';
require_once dirname ( __FILE__ ) . '/../../models/AgeGroup.php';
require_once dirname ( __FILE__ ) . '/../../models/Position.php';
require_once dirname ( __FILE__ ) . '/../../models/Acceptable.php';
require_once dirname ( __FILE__ ) . '/../../services/EngineerService.php';
session_start ();
$message = "";
$validationMessage = "";

// 期限が過ぎた仮登録レコードを削除する
$engineerService = new EngineerService ();
$engineerService->clearExpiratedTemporayEngineer ();

$engineer = null;
$accessFromMail = false;
$reEdit = false;
if (isset ( $_GET [Keys::TEMP_KEY] )) {
	$accessFromMail = true;
} else if (isset ( $_GET ["reedit"] )) {
	$reEdit = true;
}
if ($accessFromMail) {
	// メールのリンクからアクセスされた場合
	unset ( $_SESSION [Keys::ENGINEER] );
	$temporaryKey = $_GET [Keys::TEMP_KEY];
	if (! ctype_digit ( $temporaryKey )) {
		$temporaryKey = "";
		Logger::put ( "識別できないアクセスです。" );
	}
	Logger::put ( "パラメータ：temporaryKey=" . $temporaryKey );
	$engineerMailAddress = $engineerService->getMailAddressByTemporaryKey ( $temporaryKey );
	Logger::put ( "メールアドレス=" . $engineerMailAddress );
	if ($engineerMailAddress != "") {
		$engineer = $engineerService->getEngineerByMailAddress ( $engineerMailAddress );
		if ($engineer == null) {
			$engineer = new Engineer ();
			$engineer->setMailAddress ( $engineerMailAddress );
		}
		$_SESSION [Keys::ENGINEER] = $engineer;
	} else {
		$message = "タイムアウトまたは識別できないアクセスです。";
	}
} else {
	// 確認ボタン押下の場合
	if (isset ( $_SESSION [Keys::ENGINEER] )) {
		$engineer = $_SESSION [Keys::ENGINEER];
		if (isset ( $_POST ["confirmButton"] )) {
			$engineer->setDisplayName ( trim ( $_POST ["display_name"] ) );
			$engineer->setAcceptable ( $_POST ["acceptable"] );
			$engineer->setPosition ( $_POST ["position"] );
			$engineer->setAgeGroup ( $_POST ["age_group"] );
			$engineer->setPrefecture ( $_POST ["prefecture"] );
			$engineer->setCityName ( trim ( $_POST ["city_name"] ) );
			$engineer->setFreeWord ( trim ( $_POST ["free_word"] ) );
			$engineer->clearSkillList ();
			$skillList = getSkillListByPOST ();
			foreach ( $skillList as $sid ) {
				$engineer->addSkill ( $sid );
			}
			$_SESSION [Keys::ENGINEER] = $engineer;
			$validationMessage = $engineer->validate ();
			if (! $reEdit && $validationMessage == "") {
				$url = "./confirm.php";
				header ( "Location:{$url}" );
				exit ();
			}
		} else if (isset ( $_POST ["deleteButton"] )) {
			$url = "./confirmdelete.php";
			header ( "Location:{$url}" );
			exit ();
		}
	} else {
		$message = "タイムアウトしました。";
	}
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta content="" name="description">
<title><?php echo Constants::SITE_NAME ?> 登録・編集</title>
<link rel="stylesheet" type="text/css" href="/app/style.css" media="screen" title="Stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="../js/editPage.js" charset="utf-8">
</script>
<!-- VIEW PORT 2018.3.6 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<h1><?php echo Constants::SITE_NAME ?> 登録・編集</h1>
<?php
if ($engineer != null) {
	if ($engineer->isNewEngineer ()) {
		print ("[新規技術者情報の登録]<br>") ;
	} else {
		$temp = htmlspecialchars ( $engineer->getMailAddress () );
		print ("[登録済み技術者情報の編集] {$temp}<br>") ;
	}
}
if ($message != "") {
	print ("<div style=\"color:red;\">{$message}</div>") ;
	print ("<br>") ;
	print ("<button type=\"button\" onClick=\"location.href='/app/index.html'\">戻る</button>") ;
	print ("<br>") ;
} else {
	// バリデーション結果メッセージ
	if ($validationMessage != "") {
		print ("<div style=\"color:red;\">{$validationMessage}</div>") ;
	}
	?>
<div>
		<form name="form1" method="post" action="edit.php">
			<dl id="panel">
				<dt>基本情報</dt>
				<dd>
<?php
	// 表示名
	$temp = htmlspecialchars ( $engineer->getDisplayName () );
	print ("表示名[必須]： <input type=\"text\" name=\"display_name\" value=\"{$temp}\" maxlength=\"50\" size=\"20\">") ;
	print (" " . Engineer::MAX_LENGTH_DISPLAY_NAME . "文字以内<br>") ;
	// 仕事状況
	print ("仕事状況： <select name=\"acceptable\">") ;
	foreach ( Acceptable::$list as $key => $acceptableName ) {
		if ($key == $engineer->getAcceptable ()) {
			print ("<option value=\"{$key}\" selected>{$acceptableName}</option>") ;
		} else {
			print ("<option value=\"{$key}\">{$acceptableName}</option>") ;
		}
	}
	print ("</select>") ;
	print ("<br>") ;
	// 働き方
	print ("働き方： <select name=\"position\">") ;
	foreach ( Position::$list as $key => $positionName ) {
		if ($key == $engineer->getPosition ()) {
			print ("<option value=\"{$key}\" selected>{$positionName}</option>") ;
		} else {
			print ("<option value=\"{$key}\">{$positionName}</option>") ;
		}
	}
	print ("</select>") ;
	print ("<br>") ;
	// 年齢層
	print ("年齢層： <select name=\"age_group\">") ;
	foreach ( AgeGroup::$list as $key => $ageGroupName ) {
		if ($key == $engineer->getAgeGroup ()) {
			print ("<option value=\"{$key}\" selected>{$ageGroupName}</option>") ;
		} else {
			print ("<option value=\"{$key}\">{$ageGroupName}</option>") ;
		}
	}
	print ("</select>") ;
	print ("<br>") ;
	print ("居住地： <select name=\"prefecture\">") ;
	foreach ( Prefecture::$list as $key => $prefectureName ) {
		if ($key == $engineer->getPrefecture ()) {
			print ("<option value=\"{$key}\" selected>{$prefectureName}</option>") ;
		} else {
			print ("<option value=\"{$key}\">{$prefectureName}</option>") ;
		}
	}
	print ("</select>") ;
	$temp = htmlspecialchars ( $engineer->getCityName() );
	print ("&nbsp;&nbsp;市区町村： <input type=\"text\" name=\"city_name\" value=\"{$temp}\" maxlength=\"50\" size=\"20\">") ;
	print ("<br>") ;
	print ("フリーワード ）： " . engineer::MAX_LENGTH_FREE_WORD . "文字以内<br>") ;	
	$temp = htmlspecialchars ( $engineer->getFreeWord());
	print ("<textarea name=\"free_word\" align=\"left\" style=\"width:100%; box-sizing:border-box\" rows=\"3\" maxlength=\"250\">{$temp}</textarea>") ;
	?>
				</dd>

				<dt>言語</dt>
				<dd>
<?php
	foreach ( Skill::$langeageList as $skillId => $skillName ) {
		if ($engineer->hasSkill ( $skillId )) {
			print ("<label><input type=\"checkbox\" name=\"language[]\" value=\"{$skillId}\"　checked checked>{$skillName}</label>") ;
		} else {
			print ("<label><input type=\"checkbox\" name=\"language[]\" value=\"{$skillId}\">{$skillName}</label>") ;
		}
	}
	?>
				</dd>
				<dt>OS</dt>
				<dd>
<?php
	foreach ( Skill::$osList as $skillId => $skillName ) {
		if ($engineer->hasSkill ( $skillId )) {
			print ("<label><input type=\"checkbox\" name=\"os[]\" value=\"{$skillId}\"　checked checked>{$skillName}</label>") ;
		} else {
			print ("<label><input type=\"checkbox\" name=\"os[]\" value=\"{$skillId}\">{$skillName}</label>") ;
		}
	}
	?>
				</dd>
				<dt>ドメイン</dt>
				<dd>
<?php
	foreach ( Skill::$domainList as $skillId => $skillName ) {
		if ($engineer->hasSkill ( $skillId )) {
			print ("<label><input type=\"checkbox\" name=\"domain[]\" value=\"{$skillId}\"　checked checked>{$skillName}</label>") ;
		} else {
			print ("<label><input type=\"checkbox\" name=\"domain[]\" value=\"{$skillId}\">{$skillName}</label>") ;
		}
	}
	?>
				</dd>
				<dt>役割</dt>
				<dd>
<?php
	foreach ( Skill::$roleList as $skillId => $skillName ) {
		if ($engineer->hasSkill ( $skillId )) {
			print ("<label><input type=\"checkbox\" name=\"role[]\" value=\"{$skillId}\"　checked checked>{$skillName}</label>") ;
		} else {
			print ("<label><input type=\"checkbox\" name=\"role[]\" value=\"{$skillId}\">{$skillName}</label>") ;
		}
	}
	?>
				</dd>
			</dl>
			<div>
				<button type="submit" name="confirmButton">確認する</button>
				<button type="button" onClick="location.href='/app/index.html'">キャンセル</button>
				<?php
	
	if (! $engineer->isNewEngineer ()) {
		print ("<button type=\"submit\" name=\"deleteButton\">削除する</button>") ;
	}
	?>
			</div>


		</form>

	</div>
<?php }?>
<!-- FOOTER -->
<div id="ft">
&nbsp;<a href="../../index.html" target="_blank"><font color="white">トップ</font></a>
&nbsp;<a href="http://www.nextdesign.co.jp/" target="_blank"><font color="white">運営者</font></a>
&nbsp;<a href="kiyaku.html" target="_blank"><font color="white">利用規約</font></a>
</div>
</body>
</html>
<?php
function getSkillListByPOST() {
	$resultArray = array ();
	if (isset ( $_POST ["language"] )) {
		$resultArray = array_merge ( $resultArray, $_POST ["language"] );
	}
	if (isset ( $_POST ["os"] )) {
		$resultArray = array_merge ( $resultArray, $_POST ["os"] );
	}
	if (isset ( $_POST ["domain"] )) {
		$resultArray = array_merge ( $resultArray, $_POST ["domain"] );
	}
	if (isset ( $_POST ["role"] )) {
		$resultArray = array_merge ( $resultArray, $_POST ["role"] );
	}
	return $resultArray;
}
?>