<?php
require_once dirname ( __FILE__ ) . '/../../Constants.php';
require_once dirname ( __FILE__ ) . '/../Keys.php';
require_once dirname ( __FILE__ ) . '/../../util/Logger.php';
require_once dirname ( __FILE__ ) . '/../../models/Engineer.php';
require_once dirname ( __FILE__ ) . '/../../services/EngineerService.php';
session_start ();
$error = false;
$message = "登録完了しました。";
$engineer = $_SESSION [Keys::ENGINEER];
// 登録する
if ($engineer != null) {
	$engineerService = new EngineerService ();
	$updatedEngineer = $engineerService->addOrReplaceEngineer($engineer);
	if ($updatedEngineer == null){
		$error = true;
		$message = "登録に失敗しました。";
	}
} else {
	$error = true;
	$message = "登録できませんでした。";
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta content="" name="description">
<title><?php echo Constants::SITE_NAME ?> 完了</title>
<link rel="stylesheet" type="text/css" href="/app/style.css" media="screen"
	title="Stylesheet" />
<!-- VIEW PORT 2018.3.6 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<h1><?php echo Constants::SITE_NAME ?> 完了</h1>
	<div>
		<?php 
		if ($error){
			print ("<font color=\"red\">{$message}</font>");
		} else {
			print ("<font>{$message}</font>");
		}
		?>
	</div>
<!-- FOOTER -->
<div id="ft">
&nbsp;<a href="../../index.html" target="_blank"><font color="white">トップ</font></a>
&nbsp;<a href="http://www.nextdesign.co.jp/" target="_blank"><font color="white">運営者</font></a>
&nbsp;<a href="kiyaku.html" target="_blank"><font color="white">利用規約</font></a>
</div>
</body>
</html>
