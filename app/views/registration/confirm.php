<?php
require_once dirname ( __FILE__ ) . '/../../Constants.php';
require_once dirname ( __FILE__ ) . '/../Keys.php';
require_once dirname ( __FILE__ ) . '/../../util/Logger.php';
require_once dirname ( __FILE__ ) . '/../../util/StringUtil.php';
require_once dirname ( __FILE__ ) . '/../../models/Engineer.php';
require_once dirname ( __FILE__ ) . '/../../models/Skill.php';
session_start ();
$engineer = null;
if (isset ( $_SESSION [Keys::ENGINEER] )) {
	$engineer = $_SESSION [Keys::ENGINEER];
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta content="" name="description">
<title><?php echo Constants::SITE_NAME ?> 確認</title>
<link rel="stylesheet" type="text/css" href="/app/style.css" media="screen"
	title="Stylesheet" />
<!-- VIEW PORT 2018.3.6 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<h1><?php echo Constants::SITE_NAME ?> 確認</h1>
	<div>
<?php
print ("<table>") ;
print ("<tbody>") ;

print ("<tr>") ;
print ("<td>表示名</td>") ;
$temp = htmlspecialchars($engineer->getDisplayName());
print ("<td>{$temp}</td>") ;
print ("</tr>") ;

print ("<tr>") ;
print ("<td>居住地</td>") ;
$temp = htmlspecialchars($engineer->getAddressText());
print ("<td>{$temp}</td>") ;
print ("</tr>") ;

print ("<tr>") ;
print ("<td>仕事状況</td>") ;
print ("<td>{$engineer->getAcceptableText()}</td>") ;
print ("</tr>") ;

print ("<tr>") ;
print ("<td>働き方</td>") ;
print ("<td>{$engineer->getPositionText()}</td>") ;
print ("</tr>") ;

print ("<tr>") ;
print ("<td>年齢層</td>") ;
print ("<td>{$engineer->getAgeGroupText()}</td>") ;
print ("</tr>") ;

print ("<tr>") ;
print ("<td>フリーワード</td>") ;
$temp = htmlspecialchars($engineer->getFreeWord());
$temp = StringUtil::replaceCRLFToBR($temp);
print ("<td>{$temp}</td>") ;
print ("</tr>") ;

foreach ( $engineer->getLanguageSkillList () as $skillName ) {
	print ("<tr>") ;
	print ("<td>言語</td>") ;
	print ("<td>{$skillName}</td>") ;
	print ("</tr>") ;
}
foreach ( $engineer->getOsSkillList () as $skillName ) {
	print ("<tr>") ;
	print ("<td>OS</td>") ;
	print ("<td>{$skillName}</td>") ;
	print ("</tr>") ;
}
foreach ( $engineer->getDomainSkillList () as $skillName ) {
	print ("<tr>") ;
	print ("<td>分野</td>") ;
	print ("<td>{$skillName}</td>") ;
	print ("</tr>") ;
}
foreach ( $engineer->getRoleSkillList () as $skillName ) {
	print ("<tr>") ;
	print ("<td>役割</td>") ;
	print ("<td>{$skillName}</td>") ;
	print ("</tr>") ;
}
print ("</tbody>") ;
print ("</table>") ;
?>

<form name="form1" method="post" action="finish.php">
			<div>
				<button type="submit">登録する</button>
				<button type="button" onClick="location.href='./edit.php?reedit=1'">戻る</button>
			</div>
		</form>
	</div>
<!-- FOOTER -->
<div id="ft">
&nbsp;<a href="../../index.html" target="_blank"><font color="white">トップ</font></a>
&nbsp;<a href="http://www.nextdesign.co.jp/" target="_blank"><font color="white">運営者</font></a>
&nbsp;<a href="kiyaku.html" target="_blank"><font color="white">利用規約</font></a>
</div>
</body>
</html>
