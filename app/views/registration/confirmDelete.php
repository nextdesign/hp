<?php
require_once dirname ( __FILE__ ) . '/../../Constants.php';
require_once dirname ( __FILE__ ) . '/../Keys.php';
require_once dirname ( __FILE__ ) . '/../../util/Logger.php';
require_once dirname ( __FILE__ ) . '/../../models/Engineer.php';
session_start ();
$engineer = null;
if (isset ( $_SESSION [Keys::ENGINEER] )) {
	$engineer = $_SESSION [Keys::ENGINEER];
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta content="" name="description">
<title><?php echo Constants::SITE_NAME ?> 削除確認</title>
<link rel="stylesheet" type="text/css" href="/app/style.css" media="screen"
	title="Stylesheet" />
<!-- VIEW PORT 2018.3.6 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<h1><?php echo Constants::SITE_NAME ?> 削除確認</h1>
	<div>
	<font color="red" size="+1">削除しますか？</font> 
	</div>
	<div>
		<form name="form1" method="post" action="finishDelete.php">
			<div>
				<button type="submit">削除する</button>
				<button type="button" onClick="location.href='./edit.php?reedit=1'">戻る</button>
			</div>
		</form>
	</div>
<!-- FOOTER -->
<div id="ft">
&nbsp;<a href="../../index.html" target="_blank"><font color="white">トップ</font></a>
&nbsp;<a href="http://www.nextdesign.co.jp/" target="_blank"><font color="white">運営者</font></a>
&nbsp;<a href="kiyaku.html" target="_blank"><font color="white">利用規約</font></a>
</div>
</body>
</html>
