<?php
require_once dirname ( __FILE__ ) . '/../../Constants.php';
require_once dirname ( __FILE__ ) . '/../Keys.php';
require_once dirname ( __FILE__ ) . '/../../util/Logger.php';
require_once dirname ( __FILE__ ) . '/../../models/Engineer.php';
require_once dirname ( __FILE__ ) . '/../../models/RequestMessage.php';
require_once dirname ( __FILE__ ) . '/../../services/EngineerService.php';
require_once dirname ( __FILE__ ) . '/../../services/RequesterService.php';
session_start ();
$message = "";
$validationMessage = "";

// 期限が過ぎた仮登録レコードを削除する
$requesterService = new RequesterService ();
$requesterService->clearExpiratedTemporayRequester ();

$engineer = null;
$accessFromMail = false;
$reEdit = false;
if (isset ( $_GET [Keys::TEMP_KEY] )) {
	$accessFromMail = true;
} else if (isset ( $_GET ["reedit"] )) {
	$reEdit = true;
}
if ($accessFromMail) {
	// メールのリンクからアクセスされた場合
	unset ( $_SESSION [Keys::REQUEST_MESSAGE] );
	$temporaryKey = $_GET [Keys::TEMP_KEY];
	if (! ctype_digit ( $temporaryKey )) {
		$temporaryKey = "";
		Logger::put ( "識別できないアクセスです。" );
	}
	Logger::put ( "パラメータ：temporaryKey=" . $temporaryKey );
	
	$resultRecord = $requesterService->getMailAddressAndEngineerIdByTemporaryKey ( $temporaryKey );
	$requesterMailAddress = $resultRecord ["mail_address"];
	$engineerId = $resultRecord ["engineer_id"];
	Logger::put ( "リクエスタメールアドレス=" . $requesterMailAddress . " engineerid=" . $engineerId );
	$engineer = null;
	$requestMessage = null;
	if ($requesterMailAddress != "" && $engineerId != null && $engineerId > 0) {
		$engineerService = new EngineerService ();
		$engineer = $engineerService->getEngineerById ( $engineerId );
		if ($engineer != null) {
			$requestMessage = new RequestMessage();
			$requestMessage->setRequesterMailAddress($requesterMailAddress);
			$requestMessage->setEngineerId($engineer->getid());
			$requestMessage->setEngineerMailAddress($engineer->getMailAddress());
			$requestMessage->setReplyTo($requesterMailAddress);
			$_SESSION [Keys::REQUEST_MESSAGE] = $requestMessage;
		} else {
			$message = "タイムアウトまたは識別できないアクセスです。";
		}
	} else {
		$message = "タイムアウトまたは識別できないアクセスです。";
	}
} else {
	// 確認ボタン押下の場合
	if (isset ( $_SESSION [Keys::REQUEST_MESSAGE] )) {
		$requestMessage = $_SESSION [Keys::REQUEST_MESSAGE];
		if (isset ( $_POST ["confirmButton"] ) ) {
			$requestMessage->setReplyTo ( trim ( $_POST ["replyTo"] ) );
			$requestMessage->settext1 ( trim ( $_POST ["text1"] ) );
			$_SESSION [Keys::REQUEST_MESSAGE] = $requestMessage;
			$validationMessage = $requestMessage->validate ();
			if (! $reEdit && $validationMessage == "") {
				$url = "./confirmRequest.php";
				header ( "Location:{$url}" );
				exit ();
			}
		}
	} else {
		$message = "タイムアウトしました。";
	}
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta content="" name="description">
<title><?php echo Constants::SITE_NAME ?> メッセージ編集</title>
<link rel="stylesheet" type="text/css" href="/app/style.css" media="screen"
	title="Stylesheet" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="../js/editPage.js" charset="utf-8">
</script>
<!-- VIEW PORT 2018.3.6 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<h1><?php echo Constants::SITE_NAME ?> 問合せ</h1>
<?php
if ($message != "") {
	print ("<div style=\"color:red;\">{$message}</div>") ;
	print ("<br>") ;
	print ("<button type=\"button\" onClick=\"location.href='/app/index.html'\">戻る</button>") ;
	print ("<br>") ;
} else {
	// バリデーション結果メッセージ
	if ($validationMessage != "") {
		print ("<div style=\"color:red;\">{$validationMessage}</div>") ;
	}
	?>
	<div>
		<form name="form1" method="post" action="editRequest.php">
			<p>
				返信先:　技術者側はこの返信先以外は知ることができませんので、正確にお願いします。<br>
				<?php
	$temp = htmlspecialchars ( $requestMessage->getReplyTo() );
	print ("<textarea name=\"replyTo\" align=\"left\" style=\"width:100%; box-sizing:border-box\" rows=\"2\" maxlength=\"100\">{$temp}</textarea>") ;
	?>
			</p>
			<p>
				<label>本文</label>
				<?php
	$temp = htmlspecialchars ( $requestMessage->getText1() );
	print ("<textarea name=\"text1\" align=\"left\" style=\"width:100%; box-sizing:border-box\" rows=\"15\" maxlength=\"1000\">{$temp}</textarea>") ;
	?>
			</p>
			<div>
				<button type="submit" name="confirmButton">確認する</button>
				<button type="button" onClick="location.href='/app/index.html'">キャンセル</button>
			</div>
		</form>
	</div>
	<?php }?>

<!-- FOOTER -->
<div id="ft">
&nbsp;<a href="../../index.html" target="_blank"><font color="white">トップ</font></a>
&nbsp;<a href="http://www.nextdesign.co.jp/" target="_blank"><font color="white">運営者</font></a>
&nbsp;<a href="kiyaku.html" target="_blank"><font color="white">利用規約</font></a>
</div>
</body>
</html>
