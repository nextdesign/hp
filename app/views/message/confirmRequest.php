<?php
require_once dirname ( __FILE__ ) . '/../../Constants.php';
require_once dirname ( __FILE__ ) . '/../Keys.php';
require_once dirname ( __FILE__ ) . '/../../util/Logger.php';
require_once dirname ( __FILE__ ) . '/../../util/StringUtil.php';
require_once dirname ( __FILE__ ) . '/../../models/RequestMessage.php';
session_start ();
$requestMessage = null;
if (isset ( $_SESSION [Keys::REQUEST_MESSAGE] )) {
	$requestMessage = $_SESSION [Keys::REQUEST_MESSAGE];
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta content="" name="description">
<title><?php echo Constants::SITE_NAME ?> 送信内容の確認</title>
<link rel="stylesheet" type="text/css" href="/app/style.css" media="screen"
	title="Stylesheet" />
<!-- VIEW PORT 2018.3.6 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<h1><?php echo Constants::SITE_NAME ?> 送信内容の確認</h1>
	<div>
<?php
print ("[返信先]<br>") ;
$temp = htmlspecialchars($requestMessage->getReplyTo());
$temp = StringUtil::replaceCRLFToBR($temp);
print ("{$temp}<br>") ;

print ("[本文]<br>") ;
$temp = htmlspecialchars($requestMessage->getText1());
$temp = StringUtil::replaceCRLFToBR($temp);
print ("{$temp}<br><br>") ;
?>

<form name="form1" method="post" action="finishRequest.php">
			<div>
				<button type="submit">送信する</button>
				<button type="button" onClick="location.href='./editRequest.php?reedit=1'">戻る</button>
			</div>
		</form>
	</div>
<!-- FOOTER -->
<div id="ft">
&nbsp;<a href="../../index.html" target="_blank"><font color="white">トップ</font></a>
&nbsp;<a href="http://www.nextdesign.co.jp/" target="_blank"><font color="white">運営者</font></a>
&nbsp;<a href="kiyaku.html" target="_blank"><font color="white">利用規約</font></a>
</div>
</body>
</html>
