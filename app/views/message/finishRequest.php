<?php
require_once dirname ( __FILE__ ) . '/../../Constants.php';
require_once dirname ( __FILE__ ) . '/../Keys.php';
require_once dirname ( __FILE__ ) . '/../../util/Logger.php';
require_once dirname ( __FILE__ ) . '/../../models/Engineer.php';
require_once dirname ( __FILE__ ) . '/../../models/RequestMessage.php';
require_once dirname ( __FILE__ ) . '/../../services/EngineerService.php';
require_once dirname ( __FILE__ ) . '/../../mail/Mailer.php';
session_start ();
$error = false;
$message = "メッセージを送信しました。<br>本サイトのサポートはここまでです。<br>この後は技術者様からの返信を待ち、その後は直接お打合せください。";
$requestMessage = null;
if (isset ( $_SESSION [Keys::REQUEST_MESSAGE] )) {
	$requestMessage = $_SESSION [Keys::REQUEST_MESSAGE];
}
//送信する
if ($requestMessage != null) {
	$mailer = new Mailer ();
	$result = $mailer->sendRequestEngineerMessage( $requestMessage->getEngineerMailAddress(), $requestMessage->getReplyTo(), $requestMessage->getText1());
	if ($result) {
		$message = "技術者に問合せメールを送信しました。" ;
	} else {
		$error = true;
		$message = "サイトに問題が発生し、問合せメールを送信できませんでした。";
	}
} else {
	$error = true;
	$message = "サイトに問題が発生しました。";
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta content="" name="description">
<title><?php echo Constants::SITE_NAME ?> 送信完了</title>
<link rel="stylesheet" type="text/css" href="/app/style.css" media="screen"
	title="Stylesheet" />
<!-- VIEW PORT 2018.3.6 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<h1><?php echo Constants::SITE_NAME ?> 完了</h1>
	<div>
		<?php 
		if ($error){
			print ("<font color=\"red\">{$message}</font>");
		} else {
			print ("<font>{$message}</font>");
		}
		?>
	</div>
<!-- FOOTER -->
<div id="ft">
&nbsp;<a href="../../index.html" target="_blank"><font color="white">トップ</font></a>
&nbsp;<a href="http://www.nextdesign.co.jp/" target="_blank"><font color="white">運営者</font></a>
&nbsp;<a href="kiyaku.html" target="_blank"><font color="white">利用規約</font></a>
</div>
</body>
</html>
