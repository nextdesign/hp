<?php
require_once dirname ( __FILE__ ) . '/../../Constants.php';
require_once dirname ( __FILE__ ) . '/../Keys.php';
require_once dirname(__FILE__) . '/../../mail/MailAddress.php';
session_start ();
unset ( $_SESSION [Keys::ENGINEER_MAIL_ADDRESS] );
$message = "";
$error = false;
$engineerMailAddress = "";
if ($_POST != null && isset($_POST [Keys::ENGINEER_MAIL_ADDRESS])) {
	$engineerMailAddress = trim($_POST [Keys::ENGINEER_MAIL_ADDRESS]);
	$message = MailAddress::validate ( $engineerMailAddress );
	if ($message == "") {
		$_SESSION [Keys::ENGINEER_MAIL_ADDRESS] = $engineerMailAddress;
		$url = "./mail.php";
		header ( "Location:{$url}" );
		exit ();
	} else {
		$error = true;
	}
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta content="" name="description">
<title><?php echo Constants::SITE_NAME ?> 技術者用</title>
<link rel="stylesheet" type="text/css" href="/app/style.css" media="screen"
	title="Stylesheet" />
<!-- VIEW PORT 2018.3.6 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
	<h1><?php echo Constants::SITE_NAME ?> 技術者用</h1>
	<div>
<?php
if ($message != "") {
	echo "<div style=\"color:red;\">{$message}</div><br>";
}
?>
    <div>
		下のテキストボックスにメールアドレスを入力し、「メールを受け取る」ボタンを押下します。<br>
		受信したメール本文に、技術者情報の登録・編集画面のURLが記載されています。<br>
		そのURLから登録・編集画面にアクセスします。<br>
		メールアドレスが未登録の場合は、技術者の新規登録モードになります。<br>
		すでに登録済みの場合は、登録情報の更新・削除ができます。<br>
	</div>

	<form name="form1" method="post" action="entry.php">
		<p>
		<?php 
		if ($error){
			print ("<font color=\"red\">メールアドレス： <input type=\"text\" name=\"" . Keys::ENGINEER_MAIL_ADDRESS . "\" value=\"{$engineerMailAddress}\" style=\"width:70%; box-sizing:border-box\"></font>");
		} else {
			print ("メールアドレス： <input type=\"text\" name=\"" . Keys::ENGINEER_MAIL_ADDRESS . "\" value=\"{$engineerMailAddress}\" style=\"width:70%; box-sizing:border-box\">");
		}
			?>
		</p>
		<div>
			<button type="submit">メールを受け取る</button>
			<button type="button" onClick="location.href='/app/index.html'">キャンセル</button>
		</div>
	</form>
</div>
<!-- FOOTER -->
<div id="ft">
&nbsp;<a href="../../index.html" target="_blank"><font color="white">トップ</font></a>
&nbsp;<a href="http://www.nextdesign.co.jp/" target="_blank"><font color="white">運営者</font></a>
&nbsp;<a href="kiyaku.html" target="_blank"><font color="white">利用規約</font></a>
</div>
</body>
</html>
