<?php
require_once dirname ( __FILE__ ) . '/../../Constants.php';
require_once dirname ( __FILE__ ) . '/../Keys.php';
require_once dirname ( __FILE__ ) . '/../../util/Logger.php';
require_once dirname ( __FILE__ ) . '/../../services/RequesterService.php';
require_once dirname ( __FILE__ ) . '/../../models/SearchContext.php';
require_once dirname ( __FILE__ ) . '/../../models/Skill.php';
require_once dirname ( __FILE__ ) . '/../../models/Prefecture.php';
require_once dirname ( __FILE__ ) . '/../../models/AgeGroup.php';
require_once dirname ( __FILE__ ) . '/../../models/Acceptable.php';
session_start ();
$searchContext = null;
if (isset ( $_SESSION [Keys::SEARCH_CONTEXT] )) {
	// 直前までの検索条件を引き継ぐ
	$searchContext = $_SESSION [Keys::SEARCH_CONTEXT];
	$searchContext->setNewCondition ( true );
	Logger::put ( "直前までの検索条件を引き継ぐ" );
} else {
	// 新規検索
	$searchContext = new SearchContext ();
	$searchContext->setNewCondition ( true );
	$_SESSION [Keys::SEARCH_CONTEXT] = $searchContext;
	// 期限が過ぎた仮登録レコードを削除する
	$requesterService = new RequesterService ();
	$requesterService->clearExpiratedTemporayRequester ();
	Logger::put ( "新規検索" );
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta content="" name="description">
<title><?php echo Constants::SITE_NAME ?> 検索条件</title>
<link rel="stylesheet" type="text/css" href="/app/style.css" media="screen"
	title="Stylesheet" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>
$(function() {
  $('#panel > dd').hide();
  $('#panel > dt')
    .click(function(e){
      $('+dd', this).slideToggle(500);
    })
});
</script>
<!-- VIEW PORT 2018.3.6 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<h1><?php echo Constants::SITE_NAME ?> 検索条件</h1>
	<div>
		<form name="form1" method="post" action="searchResult.php">
			<dl id="panel">
				<dt>基本情報</dt>
				<dd>
<?php
$temp = htmlspecialchars ( $searchContext->getDisplayName () );
print ("表示名： <input type=\"text\" name=\"display_name\" value=\"{$temp}\" maxlength=\"50\" size=\"20\">") ;
print ("<br>") ;
print ("居住地： <select name=\"prefecture\">") ;
foreach ( Prefecture::$list as $key => $prefectureName ) {
	if ($key == $searchContext->getPrefecture ()) {
		print ("<option value=\"{$key}\" selected>{$prefectureName}</option>") ;
	} else {
		print ("<option value=\"{$key}\">{$prefectureName}</option>") ;
	}
}
print ("</select>") ;
$temp = htmlspecialchars ( $searchContext->getCityName () );
print ("&nbsp;&nbsp;市区町村： <input type=\"text\" name=\"city_name\" value=\"{$temp}\" maxlength=\"50\" size=\"20\">") ;
print ("<br>") ;
print ("フリーワード ： 複数ワードの場合は半角カンマ区切りで指定します<br>") ;
$temp = htmlspecialchars ( $searchContext->getFreeWord () );
print ("<input type=\"text\" name=\"free_word\" value=\"{$temp}\" maxlength=\"200\" style=\"width:100%; box-sizing:border-box\">") ;
?>
				</dd>

				<dt>言語</dt>
				<dd>
<?php
foreach ( Skill::$langeageList as $skillId => $skillName ) {
	if ($searchContext->hasSkill ( $skillId )) {
		print ("<label><input type=\"checkbox\" name=\"language[]\" value=\"{$skillId}\"　checked checked>{$skillName}</label>") ;
	} else {
		print ("<label><input type=\"checkbox\" name=\"language[]\" value=\"{$skillId}\">{$skillName}</label>") ;
	}
}
?>
				</dd>
				<dt>OS</dt>
				<dd>
<?php
foreach ( Skill::$osList as $skillId => $skillName ) {
	if ($searchContext->hasSkill ( $skillId )) {
		print ("<label><input type=\"checkbox\" name=\"os[]\" value=\"{$skillId}\"　checked checked>{$skillName}</label>") ;
	} else {
		print ("<label><input type=\"checkbox\" name=\"os[]\" value=\"{$skillId}\">{$skillName}</label>") ;
	}
}
?>
				</dd>
				<dt>ドメイン</dt>
				<dd>
<?php
foreach ( Skill::$domainList as $skillId => $skillName ) {
	if ($searchContext->hasSkill ( $skillId )) {
		print ("<label><input type=\"checkbox\" name=\"domain[]\" value=\"{$skillId}\"　checked checked>{$skillName}</label>") ;
	} else {
		print ("<label><input type=\"checkbox\" name=\"domain[]\" value=\"{$skillId}\">{$skillName}</label>") ;
	}
}
?>
				</dd>
				<dt>役割</dt>
				<dd>
<?php
foreach ( Skill::$roleList as $skillId => $skillName ) {
	if ($searchContext->hasSkill ( $skillId )) {
		print ("<label><input type=\"checkbox\" name=\"role[]\" value=\"{$skillId}\"　checked checked>{$skillName}</label>") ;
	} else {
		print ("<label><input type=\"checkbox\" name=\"role[]\" value=\"{$skillId}\">{$skillName}</label>") ;
	}
}
?>
				</dd>
			</dl>
			<div>
				<button type="submit" name="searchButton">検索する</button>
				<button type="button" onClick="location.href='/app/index.html'">終了する</button>
				<?php
				
				?>
			</div>

		</form>
	</div>

<!-- FOOTER -->
<div id="ft">
&nbsp;<a href="../../index.html" target="_blank"><font color="white">トップ</font></a>
&nbsp;<a href="http://www.nextdesign.co.jp/" target="_blank"><font color="white">運営者</font></a>
&nbsp;<a href="kiyaku.html" target="_blank"><font color="white">利用規約</font></a>
</div>
</body>
</html>
