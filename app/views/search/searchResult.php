<?php
require_once dirname ( __FILE__ ) . '/../../Constants.php';
require_once dirname ( __FILE__ ) . '/../Keys.php';
require_once dirname ( __FILE__ ) . '/../../util/Logger.php';
require_once dirname ( __FILE__ ) . '/../../util/StringUtil.php';
require_once dirname ( __FILE__ ) . '/../../services/EngineerService.php';
require_once dirname ( __FILE__ ) . '/../../models/SearchContext.php';
require_once dirname ( __FILE__ ) . '/../../models/Skill.php';
session_start ();
const EMPTY_CONDITION_ERROR = "検索条件をひとつ以上設定してください。";
const PAGE_BUTTON_NAME = "pageButton";
const PREV_BUTTON_NAME = "prevButton";
const NEXT_BUTTON_NAME = "nextButton";
$message = "";
$error = false;
$searchContext = null;
if (isset ( $_SESSION [Keys::SEARCH_CONTEXT] )) {
	$searchContext = $_SESSION [Keys::SEARCH_CONTEXT];
}
if ($searchContext != null) {
	if ($searchContext->isNewCondition ()) {
		if ($_POST != null) {
			// 条件画面の設定内容を反映する
			Logger::put ( "条件画面の設定内容を反映する" );
			$searchContext->setDisplayName ( $_POST ["display_name"] );
			$searchContext->setPrefecture ( $_POST ["prefecture"] );
			$searchContext->setCityName ( trim ( $_POST ["city_name"] ) );
			$searchContext->setFreeWord ( trim ( $_POST ["free_word"] ) );
			$searchContext->clearSkillList ();
			$skillList = getSkillListByPOST ();
			foreach ( $skillList as $sid ) {
				$searchContext->addSkill ( $sid );
			}
			$_SESSION [Keys::SEARCH_CONTEXT] = $searchContext;
			if ($searchContext->isValidCondition ()) {
				$engineerService = new EngineerService ();
				$count = $engineerService->countTotalRows ( $searchContext );
				$searchContext->setTotalRowCount ( $count );
				$searchContext->setCurrentPageNo ( 1 );
				$searchContext->setNavigatorStartPageNo ( 1 );
				$searchContext->setNewCondition ( false );
				Logger::put ( "トータル件数=" . $count );
			} else {
				$error = true;
				$message = EMPTY_CONDITION_ERROR;
			}
		}
	} else if (isset ( $_POST [PAGE_BUTTON_NAME] )) {
		$searchContext->setCurrentPageNo ( $_POST [PAGE_BUTTON_NAME] );
		Logger::put ( "ページ選択" . $searchContext->getCurrentPageNo () );
	} else {
		if (isset ( $_POST [PREV_BUTTON_NAME] )) {
			$searchContext->prevNavigator ();
			Logger::put ( "前ページへ 表示開始=" . $searchContext->getNavigatorStartPageNo () );
		} else if (isset ( $_POST [NEXT_BUTTON_NAME] )) {
			$searchContext->nextNavigator ();
			Logger::put ( "次ページへ 表示開始=" . $searchContext->getNavigatorStartPageNo () );
		}
	}
} else {
	$error = true;
	$message = "タイムアウトしました。";
}

// 検索
$engineerService = new EngineerService ();
$engineerList = $engineerService->search ( $searchContext );

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta content="" name="description">
<title><?php echo Constants::SITE_NAME ?> 検索結果</title>
<link rel="stylesheet" type="text/css" href="/app/style.css" media="screen"
	title="Stylesheet" />
<!-- VIEW PORT 2018.3.6 -->
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<h1><?php echo Constants::SITE_NAME ?> 検索結果</h1>
	<p>左側の「問合せ」リンクから問合せメールを送信できます。</p>
	<?php
	if ($message != "") {
		print ("<div><font color =\"red\">{$message}</font></div>") ;
		if ($error) {
			print ("<div>") ;
			print ("<button type=\"button\" onClick=\" location.href='./searchCondition.php'\">条件を変更する</button>&nbsp;") ;
			print ("<button type=\"button\" onClick=\" location.href='/app/index.html'\">終了する</button>") ;
			print ("</div>") ;
		}
	} else {
		print ("検索結果全{$searchContext->getTotalRowCount()}件中 {$searchContext->getStartRowNo()}件目-{$searchContext->getEndRowNo()}件目を表示") ;
		?>
<div>
		<table class="engineerList"> 
	<?php
		foreach ( $engineerList as $engineer ) {
			print ("<tr>") ;
			print ("<td rowspan=\"6\" valign=\"top\"><a href=\"../requester/contact.php?" . Keys::CONTACT_TO . "={$engineer->getId()}\">問合せ</a></td>") ;
			$temp = htmlspecialchars ( $engineer->getBasicInfo () );
			print ("<td width=\"90%\">基本情報: {$temp}</td>") ;
			print ("</tr>") ;
			print ("<tr>") ;
			$temp = htmlspecialchars ( $engineer->getFreeWord () );
			print ("<td width=\"90%\">フリーワード: {$temp}</td>") ;
			print ("</tr>") ;
			print ("<tr>") ;
			$temp = StringUtil::joinWithComma ( $engineer->getLanguageSkillList () );
			print ("<td width=\"90%\">言語: {$temp}</td>") ;
			print ("</tr>") ;
			print ("<tr>") ;
			$temp = StringUtil::joinWithComma ( $engineer->getOsSkillList () );
			print ("<td width=\"90%\">OS: {$temp}</td>") ;
			print ("</tr>") ;
			print ("<tr>") ;
			$temp = StringUtil::joinWithComma ( $engineer->getDomainSkillList () );
			print ("<td width=\"90%\">ドメイン: {$temp}</td>") ;
			print ("</tr>") ;
			print ("<tr>") ;
			$temp = StringUtil::joinWithComma ( $engineer->getRoleSkillList () );
			print ("<td width=\"90%\">役割: {$temp}</td>") ;
			print ("</tr>") ;
		}
		?>
</table>
		<br>
	</div>
	<div>
		<form name="form1" method="post" action="searchResult.php">
			<div>
			<?php
		if ($searchContext->hasPrevPage ()) {
			print ("<button type=\"submit\" name=\"" . PREV_BUTTON_NAME . "\">前へ</button>") ;
		}
		for($i = 0; $i < SearchContext::NAVIGATOR_WIDTH; $i ++) {
			$no = $i + $searchContext->getNavigatorStartPageNo ();
			if ($no <= $searchContext->getMaxPageNo ()) {
				if ($searchContext->getCurrentPageNo () == $no) {
					print ("<button class=\"pagination-button\" style=\"background-color:gray;\" type=\"submit\" name=\"" . PAGE_BUTTON_NAME . "\" value=\"{$no}\">{$no}</button>") ;
				} else {
					print ("<button class=\"pagination-button\" style=\"background-color:white;\" type=\"submit\" name=\"" . PAGE_BUTTON_NAME . "\" value=\"{$no}\">{$no}</button>") ;
				}
			}
		}
		if ($searchContext->hasNextPage ()) {
			print ("<button type=\"submit\" name=\"" . NEXT_BUTTON_NAME . "\">次へ</button>") ;
		}
		?>
				<button type="button" onClick=" location.href='./searchCondition.php'">条件を変更する</button>
				<button type="button" onClick=" location.href='/app/index.html'">終了する</button>
			</div>
		</form>
	</div>
	<?php
	} // if ($message != "")
	?>

<!-- FOOTER -->
<div id="ft">
&nbsp;<a href="../../index.html" target="_blank"><font color="white">トップ</font></a>
&nbsp;<a href="http://www.nextdesign.co.jp/" target="_blank"><font color="white">運営者</font></a>
&nbsp;<a href="kiyaku.html" target="_blank"><font color="white">利用規約</font></a>
</div>
</body>
</html>

<?php
function getSkillListByPOST() {
	$resultArray = array ();
	if (isset ( $_POST ["language"] )) {
		$resultArray = array_merge ( $resultArray, $_POST ["language"] );
	}
	if (isset ( $_POST ["os"] )) {
		$resultArray = array_merge ( $resultArray, $_POST ["os"] );
	}
	if (isset ( $_POST ["domain"] )) {
		$resultArray = array_merge ( $resultArray, $_POST ["domain"] );
	}
	if (isset ( $_POST ["role"] )) {
		$resultArray = array_merge ( $resultArray, $_POST ["role"] );
	}
	return $resultArray;
}
?>